package edu.ntust.cs.rfid.hw3;

import android.nfc.NdefMessage;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public interface NFCHandler {

	public boolean isSupported();

	public boolean isEnable();

	public boolean isNDEFPushEnable();

	public void disableForeground();

	public void enableForegrount();
	
	

	public void setNdefPushMessage(NdefMessage message);

}
