package edu.ntust.cs.rfid.hw3;

/**
 * 
 * @author Suc-Wei Tang
 *
 */

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.primitives.Bytes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.Tag;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.Contacts.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class NFCMain extends Activity implements CreateNdefMessageCallback,
OnNdefPushCompleteCallback {

	private NFCHandler nfcHandler;
	private PendingIntent pendintent;
	private IntentFilter[] filtersarr;
	private TextView tv_type;
	private TextView tv_size;
	private TextView tv_content;
	private Button btn_scanTag;
	IntentFilter[] gWriteTagFilters;
	private boolean tagCall = false;
	private String NDEFType;
	private int NDEFSize = 0;
	private String MsgType;
	private String Payload ;
	private String MsgId;
	private int MsgSize = 0;
	private short MsgTnf = 0;
	private String MessageContent ;
	private AlertDialog alertDialog;
	private Button btn_tab_receiving;
	private Button btn_tab_beaming;
	private LinearLayout li_read;
	private LinearLayout li_write;
	private boolean tag_write = false;
	private boolean tag_scan = false;
	private boolean tag_onResume = false;
	private String NdefTNF = "";
	private String writeResult = "";
	private EditText write_edt_text ;
	private TextView read_tv_text;
	private TextView tv_check;
	private Byte uri_prefix ;
	private String txtRndcoding = "";
	private NfcAdapter mNfcAdapter;
	private static final int MESSAGE_SENT = 1;
	private static final BiMap<Byte, String> URI_PREFIX_MAP = ImmutableBiMap
			.<Byte, String> builder().put((byte) 0x00, "")
			.put((byte) 0x01, "http://www.").put((byte) 0x02, "https://www.")
			.put((byte) 0x03, "http://").put((byte) 0x04, "https://")
			.put((byte) 0x05, "tel:").put((byte) 0x06, "mailto:")
			.put((byte) 0x07, "ftp://anonymous:anonymous@")
			.put((byte) 0x08, "ftp://ftp.").put((byte) 0x09, "ftps://")
			.put((byte) 0x0A, "sftp://").put((byte) 0x0B, "smb://")
			.put((byte) 0x0C, "nfs://").put((byte) 0x0D, "ftp://")
			.put((byte) 0x0E, "dav://").put((byte) 0x0F, "news:")
			.put((byte) 0x10, "telnet://").put((byte) 0x11, "imap:")
			.put((byte) 0x12, "rtsp://").put((byte) 0x13, "urn:")
			.put((byte) 0x14, "pop:").put((byte) 0x15, "sip:")
			.put((byte) 0x16, "sips:").put((byte) 0x17, "tftp:")
			.put((byte) 0x18, "btspp://").put((byte) 0x19, "btl2cap://")
			.put((byte) 0x1A, "btgoep://").put((byte) 0x1B, "tcpobex://")
			.put((byte) 0x1C, "irdaobex://").put((byte) 0x1D, "file://")
			.put((byte) 0x1E, "urn:epc:id:").put((byte) 0x1F, "urn:epc:tag:")
			.put((byte) 0x20, "urn:epc:pat:").put((byte) 0x21, "urn:epc:raw:")
			.put((byte) 0x22, "urn:epc:").put((byte) 0x23, "urn:nfc:").build();

	private TextView tv_result;
	private String data = "";
	private NdefMessage NDEFMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_main);
//		nfcHandler = new NFCHandlerImpl(this);
		findView();
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		NFCinit();
		NECDeviceCheck();

	}

	private void NFCinit() {
		pendintent = PendingIntent.getActivity(this, 0, new Intent(this,this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
		IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		filtersarr = new IntentFilter[] { tagDetected };
	}
	
	@Override
	protected void onResume() {
		super.onResume();
//		if (!nfcHandler.isSupported()) {
//			tv_check.setText(R.string.nfc_unsupported);
//
//		} else {
//			if (!nfcHandler.isEnable()) {
//				tv_check.setText(R.string.nfc_disable);
//
//			} else if (!nfcHandler.isNDEFPushEnable()) {
//				tv_check.setText(R.string.nfc_ndefpush_disable);
//
//			}
//		}

		System.out.println("NFCRead=============Resume============="+ getIntent().getAction());
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
			Tag detectTag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
			if (tag_write == true) {
				System.out.println("==============write=============" + data);

				// true 為 utf-8編碼
				// NDEFMsg = getNdefMsg_from_RTD_TEXT(data, true, false);

				// writeTag(NDEFMsg, detectTag);

//				NDEFMsg = getNdefMsg_from_RTD_URI(data, (byte) uri_prefix,
//						false);
//				nfcHandler.setNdefPushMessage(NDEFMsg);
				
//				mNfcAdapter.setNdefPushMessage(NDEFMsg, NFCMain.this);

				// tv_result.setText(writeResult);

			} else {
				System.out.println("==============read=============");
				parserIntent(getIntent());
				// setIntent(new Intent());
			}

		}
		if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction())) {
			// Tag detectTag =
			// getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);

		}

//		nfcHandler.enableForegrount();
		mNfcAdapter.enableForegroundDispatch(this, pendintent, filtersarr, null);

	}

	private void NECDeviceCheck() {
		if (mNfcAdapter == null) {
			tv_check.setText(R.string.nfc_unsupported);
		} else {

			if (!mNfcAdapter.isEnabled()) {
				tv_check.setText(R.string.nfc_disable);
			} else if (!mNfcAdapter.isNdefPushEnabled()) {
				tv_check.setText(R.string.nfc_ndefpush_disable);
			} else {
				mNfcAdapter.setNdefPushMessageCallback(this, this);
				mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		tagCall = false;
		mNfcAdapter.disableForegroundDispatch(this);
//		nfcHandler.disableForeground();
		if (alertDialog != null) {
			alertDialog.cancel();
			// tag_scan=false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void findView() {
		// tv_size = (TextView) findViewById(R.id.nfc_main_read_tv_size);
		// tv_type = (TextView) findViewById(R.id.nfc_main_read_tv_type);
		tv_content = (TextView) findViewById(R.id.nfc_main_read_tv_content);
		btn_scanTag = (Button) findViewById(R.id.nfc_main_btn_scanTag);
		btn_tab_receiving = (Button) findViewById(R.id.tab_btn_nfc_receiving);
		btn_tab_beaming = (Button) findViewById(R.id.tab_btn_nfc_beaming);
		li_read = (LinearLayout) findViewById(R.id.nfc_main_li_read);
		li_write = (LinearLayout) findViewById(R.id.nfc_main_li_write);
		// tv_result = (TextView) findViewById(R.id.nfc_main_write_tv_result);
		tv_check = (TextView) findViewById(R.id.nfc_main_tv_check);
		write_edt_text = (EditText) findViewById(R.id.nfc_main_write_edt_text);
		write_edt_text.setText("http://developer.android.com/");
		tag_write = true;
		li_read.setVisibility(View.GONE);
		li_write.setVisibility(View.VISIBLE);
		btn_tab_receiving.setOnClickListener(tabListener);
		btn_tab_beaming.setOnClickListener(tabListener);
		btn_tab_receiving.setTag("tab_read");
		btn_tab_beaming.setTag("tab_write");
		btn_scanTag.setText("Beaming Message");
		btn_tab_receiving.setBackgroundColor(getResources().getColor(R.color.gray));
		btn_tab_beaming.setBackgroundColor(getResources().getColor(	R.color.gray2));
		btn_scanTag.setOnClickListener(sanTagListener);

	}

	@Override
	public void onNewIntent(Intent intent) {
		if (tag_scan == true) {
			tag_scan = false;
			Vibrator myVibrator = (Vibrator) getApplication().getSystemService(Service.VIBRATOR_SERVICE);
			myVibrator.vibrate(500);
			setIntent(intent);
		}
		tagCall = true;
		System.out.println(tag_scan + "====onNewIntent==" + intent.getAction());
	}

	private void parserIntent(Intent intent) {

		System.out.println("==============ParserIntent===================");
		NdefMessage[] msgs = null;
		// Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		// Ndef getNdef = Ndef.get(tag);
		// NDEFType = getNdefType(getNdef.getType());
		// NDEFSize = getNdef.getMaxSize();
		// tv_type.setText(NDEFType);
		// tv_size.setText(NDEFSize + "Byte");

		Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

		if (rawMsgs != null) {
			msgs = new NdefMessage[rawMsgs.length];
			for (int i = 0; i < rawMsgs.length; i++) {
				msgs[i] = (NdefMessage) rawMsgs[i];
				MsgSize += msgs[i].toByteArray().length;
				System.out.println("msg[" + i + "]" + msgs[i]);
			}
		} else {
			// Unknown tag type
			byte[] empty = new byte[] {};
			NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty,empty, empty);
			NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
			msgs = new NdefMessage[] { msg };
		}
		processMsg(msgs);
	}

	private String getNdefType(String type) {
		// TODO Auto-generated method stub
		String myNdefType = "";

		if (type.toString().equals("com.nxp.ndef.mifareclassic")) {
			myNdefType = "NDEF on MIFARE Classic";
		} else if (type.toString().equals("org.nfcforum.ndef.type1")) {
			myNdefType = "NFC Forum Tag Type 1";
		} else if (type.toString().equals("org.nfcforum.ndef.type2")) {
			myNdefType = "NFC Forum Tag Type 2";

		} else if (type.toString().equals("org.nfcforum.ndef.type3")) {
			myNdefType = "NFC Forum Tag Type 3";
		} else if (type.toString().equals("org.nfcforum.ndef.type4")) {
			myNdefType = "NFC Forum Tag Type 4";
		}

		return myNdefType;
	}

	private void processMsg(NdefMessage[] msgs) {
		if (msgs == null || msgs.length == 0) {
			System.out.println("NdefMag==null");
		} else {
			for (int i = 0; i < msgs.length; i++) {
				int msglen = msgs[i].getRecords().length;
				NdefRecord[] records = msgs[i].getRecords();
				for (int j = 0; j < msglen; j++) // 幾個記錄
				{
					for (NdefRecord record : records) {
						if (record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							if (Arrays.equals(record.getType(),	NdefRecord.RTD_URI)) {
								Payload = parserWellKnownRtdUriPayload(record);
							} else if (Arrays.equals(record.getType(),NdefRecord.RTD_TEXT)) {
								Payload = WellKnownRtdTextPayload(record);
							}
						}
						if (record.getTnf() == NdefRecord.TNF_ABSOLUTE_URI) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						if (record.getTnf() == NdefRecord.TNF_MIME_MEDIA) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						if (record.getTnf() == NdefRecord.TNF_EXTERNAL_TYPE) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						NdefTNF = getNDEF(record.getTnf());
						MsgType = getRecordType(new String(record.getType()));
						MsgId = new String(record.getId());
						MessageContent = NdefTNF + "      " + MsgType + "\n\n";
						tv_content.setText(MessageContent);
						tv_content.append(Payload);
						tag_scan = false;
						if (tagCall == true&& record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							call(Payload);
						}
					}
				}

			}
		}

	}

	private String WellKnownRtdTextPayload(NdefRecord record) {
		String myPayload = "";
		Preconditions.checkArgument(Arrays.equals(record.getType(),	NdefRecord.RTD_TEXT));
		byte[] payload = record.getPayload();
		Byte FirstByte = record.getPayload()[0];
		// bit 7 看編碼
		txtRndcoding = ((FirstByte & 0200) == 0) ? "UTF-8" : "UTF-16";
		// mylocale
		int langCodeLen = FirstByte & 0077;
		String langCode = new String(payload, 1, langCodeLen,Charset.forName("UTF-8"));
		try {
			myPayload = new String(payload, langCodeLen + 1, payload.length- langCodeLen - 1, txtRndcoding);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("txtRndcoding==" + txtRndcoding + "  "+ "langCodeLen  " + langCodeLen);

		return myPayload;
	}

	private String parserABSOLUTE_URIPayload(NdefRecord record) {
		byte[] payload = record.getPayload();
		Uri uri = Uri.parse(new String(payload, Charset.forName("UTF-8")));
		return uri.toString();
	}

	private String getRecordType(String type) {
		String myRecordType = "";
		if (type.equals("T"))
			myRecordType = "TEXT";
		else if (type.equals("U"))
			myRecordType = "URI";
		else if (type.equals("Sp"))
			myRecordType = "SMART_POSTER";
		else if (type.equals("ac"))
			myRecordType = "ALTERNATIVE_CARRIER";
		else if (type.equals("Hc"))
			myRecordType = "HANDOVER_CARRIER";
		else if (type.equals("Hr"))
			myRecordType = "HANDOVER_REQUEST";
		else if (type.equals("Hs"))
			myRecordType = "HANDOVER_SELECT";
		else {
			myRecordType = "";
		}
		return myRecordType;
	}

	private String getNDEF(short tnf) {
		String myNdefTNF = "";
		if (tnf == NdefRecord.TNF_EMPTY) // 000
		{
			myNdefTNF = "TNF_EMPTY";
		}
		if (tnf == NdefRecord.TNF_WELL_KNOWN) // 001
		{
			myNdefTNF = "TNF_WELL_KNOWN";
		}
		if (tnf == NdefRecord.TNF_MIME_MEDIA)// 002
		{
			myNdefTNF = "TNF_MIME_MEDIA";
		}
		if (tnf == NdefRecord.TNF_ABSOLUTE_URI)// 003
		{
			myNdefTNF = "TNF_ABSOLUTE_URI";
		}
		if (tnf == NdefRecord.TNF_EXTERNAL_TYPE) // 004
		{
			myNdefTNF = "TNF_UNKNOWN";
		}
		if (tnf == NdefRecord.TNF_UNKNOWN) // 005
		{
			myNdefTNF = "TNF_UNKNOWN";
		}
		if (tnf == NdefRecord.TNF_UNCHANGED) // 006
		{
			myNdefTNF = "TNF_UNCHANGED";
		}
		return myNdefTNF;
	}

	private void call(String myPayload) {
		if (myPayload.startsWith("http://", 0)) {
			Intent intent_browser = new Intent(Intent.ACTION_VIEW,Uri.parse(myPayload));
			// Intent intentcall = new Intent(CALL, Uri.parse(myPayload));
			startActivity(intent_browser);
			System.out.println("======call=======" + tagCall);
		}
	}

	private String parserWellKnownRtdUriPayload(NdefRecord record) {
		Preconditions.checkArgument(Arrays.equals(record.getType(),	NdefRecord.RTD_URI));
		byte[] payload = record.getPayload();
		String prefix = URI_PREFIX_MAP.get(payload[0]);
		byte[] fullUri = Bytes.concat(prefix.getBytes(Charset.forName("UTF-8")),Arrays.copyOfRange(payload, 1, payload.length));
		Uri uri = Uri.parse(new String(fullUri, Charset.forName("UTF-8")));
		return uri.toString();

	}
	private Button.OnClickListener sanTagListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
//			nfcHandler.enableForegrount();
			mNfcAdapter.enableForegroundDispatch(NFCMain.this, pendintent, filtersarr, null);		
			tag_scan = true;
			// Beam write message
			if (tag_write == true) {
				if (!write_edt_text.getText().toString().equals("")) {
					data = write_edt_text.getText().toString().trim();
					if (data.startsWith("http://", 0)) {
						data = data.replace("http://", "");
						uri_prefix = 0x03;
					}
					if (data.startsWith("https://", 0)) {
						data = data.replace("https://", "");
						uri_prefix = 0x04;
					}
					NDEFMsg = getNdefMsg_from_RTD_URI(data, (byte) uri_prefix,false);
//					nfcHandler.setNdefPushMessage(NDEFMsg);
//					mNfcAdapter.setNdefPushMessage(NDEFMsg, NFCMain.this);
					System.out.println("=========Scan========");

				} else {
					Toast.makeText(NFCMain.this, "The website URL is null",	Toast.LENGTH_SHORT).show();
				}
			}

			AlertDialog.Builder builder = new AlertDialog.Builder(NFCMain.this);
			builder.setTitle("Bring the two devices together");
			// builder.setMessage("Approach a NFC Tag");
			builder.setIcon(getResources().getDrawable(R.drawable.dialogicon));
			builder.setPositiveButton("Cancel",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
//							nfcHandler.disableForeground();
							mNfcAdapter.disableForegroundDispatch(NFCMain.this);
							alertDialog.cancel();
						}
					});

			if (write_edt_text.getText().toString().equals("")&& tag_write == true) {
			} else {
				alertDialog = builder.create();
				alertDialog.setCanceledOnTouchOutside(false);
				alertDialog.show();
			}

		}
	};

	private Button.OnClickListener tabListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {			
			// read message
			if (v.getTag().toString().equals("tab_read")) {
				btn_scanTag.setText("Receiving Message");
				tag_write = false;
				li_read.setVisibility(View.VISIBLE);
				li_write.setVisibility(View.GONE);
				btn_tab_receiving.setBackgroundColor(getResources().getColor(R.color.gray2));
				btn_tab_beaming.setBackgroundColor(getResources().getColor(	R.color.gray));
			}

			// write message
			if (v.getTag().toString().equals("tab_write")) {
				btn_scanTag.setText("Beaming Message");
				tag_write = true;
				li_read.setVisibility(View.GONE);
				li_write.setVisibility(View.VISIBLE);

				btn_tab_beaming.setBackgroundColor(getResources().getColor(	R.color.gray2));
				btn_tab_receiving.setBackgroundColor(getResources().getColor(R.color.gray));
				// edt_tel.setText("");
			}

		}
	};

	// 建立TNF_WELL_KNOWN with RTD_URI (tel)
	public static NdefMessage getNdefMsg_from_RTD_URI(String uriFiledStr,byte identifierCode, boolean flagAddAAR) {

		byte[] uriField = uriFiledStr.getBytes(Charset.forName("US-ASCII"));
		byte[] payLoad = new byte[uriField.length + 1];
		payLoad[0] = identifierCode;
		System.arraycopy(uriField, 0, payLoad, 1, uriField.length);
		NdefRecord rtdUriRecord1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,NdefRecord.RTD_URI, new byte[0], payLoad);
		return new NdefMessage(new NdefRecord[] { rtdUriRecord1 });

	}

	// 建立TNF_WELL_KNOWN with RTD_TEXT
	public static NdefMessage getNdefMsg_from_RTD_TEXT(String dataStr,boolean encodeIntUtf8, boolean flagAddAAR) 
	{
		// Locale mylocale=new Locale("en","US");
		// Locale mylocale=new Locale("zh-tw","TW");
		Locale mylocale = new Locale("zh", "TW");
		// 取得預設的編碼格式
		byte[] langBytes = mylocale.getLanguage().getBytes(Charset.forName("US-ASCII"));
		// 準備轉換成UTF-8的編碼
		Charset utfEncoding = encodeIntUtf8 ? Charset.forName("UTF-8"): Charset.forName("UTF-16");
		// 往下做字元轉換的位移
		int utfBit = encodeIntUtf8 ? 0 : (1 << 7);
		char status = (char) (utfBit + langBytes.length);
		byte[] textBytes = dataStr.getBytes(utfEncoding);
		byte[] data = new byte[1 + langBytes.length + textBytes.length];
		data[0] = (byte) status;
		System.arraycopy(langBytes, 0, data, 1, langBytes.length);
		System.arraycopy(textBytes, 0, data, 1 + langBytes.length,textBytes.length);
		NdefRecord textRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,NdefRecord.RTD_TEXT, new byte[0], data);
		if (flagAddAAR) {
			// note: returns AAR for different app (nfcreadtag)
			return new NdefMessage(	new NdefRecord[] {textRecord,NdefRecord.createApplicationRecord("edu.ntust.cs.rfid.hw3") });
		} else {
			return new NdefMessage(new NdefRecord[] { textRecord });
		}

	}



	
	@Override
	public NdefMessage createNdefMessage(NfcEvent event) {
		// TODO Auto-generated method stub
		
		System.out.println("=====createNdefMessage==============="+data+tag_write);
		if (tag_write == true&&tag_scan==true) 
		{
		NDEFMsg = getNdefMsg_from_RTD_URI(data, (byte) uri_prefix,
				false);
		}
		return NDEFMsg;
	}
	
	@Override
	public void onNdefPushComplete(NfcEvent event) {
		// TODO Auto-generated method stub
		System.out.println("=====onNdefPushComplete==============="+data);
		 mHandler.obtainMessage(MESSAGE_SENT).sendToTarget();
	}
	
	 private final Handler mHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {
	            switch (msg.what) {
	            case MESSAGE_SENT:
	            	if (tag_write == true&&tag_scan==true) 
					{
	                Toast.makeText(getApplicationContext(), "Message beamed!", Toast.LENGTH_LONG).show();
					}
	                break;
	            }
	        }
	    };

	

}
