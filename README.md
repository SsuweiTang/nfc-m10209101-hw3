### NFC P2P 模式 App (Android) ###

版本  1.0

* Beaming Message
* 可建立TNF_WELL_KNOWN with RTD_URI (http/https) 型態的Record。
* 透過Android Beam 發送NDEFMessage。
* Receiving Message 
* 透過Android Beam 接收NDEFMessage。當接收的URI 格式為http/https 時，可指定瀏覽器前往該網頁。